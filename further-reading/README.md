This section includes some reading suggestions.

First of all, **the book of this lesson**: Operating System Concepts, 9th Edition by Abraham Silberschatz, Peter Baer Galvin and Greg Gagne

But also there are a few good operating system books.

* Modern Operating Systems by Andrew S. Tanenbaum
* Operating Systems In Depth: Design and Programming by Thomas W. Doeppner
* Operating Systems: Principles and Practice by Tom Anderson

_______________

#### About System Calls:
* [What is Syscall?](https://www.geeksforgeeks.org/operating-system-introduction-system-call/)
* [Python OS library](https://docs.python.org/3/library/os.html)
* [A good question](https://stackoverflow.com/questions/33654579/in-an-operating-system-what-is-the-difference-between-a-system-call-and-an-inte)

______________________________

#### About system boot process:

* [Linux Boot Process (Startup Sequence)](https://www.thegeekstuff.com/2011/02/linux-boot-process)
* [GRUB](https://wiki.archlinux.org/index.php/GRUB)
* [The boot process](https://gyires.inf.unideb.hu/GyBITT/20/ch02s02.html)


______________________________

#### Others:
* [Kernel and Types of Kernels](https://github.com/nu11secur1ty/Kernel-and-Types-of-kernels/blob/master/Kernel%20and%20Types%20of%20kernels.md)
* [Meltdown and Spectre](https://meltdownattack.com/)